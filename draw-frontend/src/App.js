import React, {Component} from 'react';
import './App.css';

class App extends Component {
    state = {
        mouseDown: false,
        pixelsArray: [],
        color: 'black'
    };

    componentDidMount() {
        this.websocket = new WebSocket('ws://localhost:8000/draw');

        this.websocket.onmessage = (message) => {
            const decodedMessage = JSON.parse(message.data);

            switch (decodedMessage.type) {
                case 'NEW_PIXELS':
                    const pixels = decodedMessage.pixels;
                    let c = document.getElementById("myCanvas");
                    let ctx = c.getContext("2d");

                    for (let i = 0; i < pixels.length; i++) {
                        ctx.beginPath();
                        ctx.arc(pixels[i].x, pixels[i].y, 10, 0, 2 * Math.PI);
                        ctx.fill();
                        ctx.fillStyle = pixels[i].color;
                    }
                    break;
                default:
                    break;
            }
        };
    }

    canvasMouseMoveHandler = event => {

        if (this.state.mouseDown) {
            event.persist();
            this.setState(prevState => {
                return {
                    pixelsArray: [...prevState.pixelsArray, {
                        x: event.clientX,
                        y: event.clientY,
                        color: this.state.color
                    }]
                };
            });

            let c = document.getElementById("myCanvas");
            let ctx = c.getContext("2d");
            ctx.beginPath();
            ctx.arc(event.clientX, event.clientY, 10, 0, 2 * Math.PI);
            ctx.fill();
            ctx.fillStyle = this.state.color;
        }
    };

    mouseDownHandler = event => {
        this.setState({mouseDown: true});
    };

    mouseUpHandler = event => {
        const message = JSON.stringify({
            type: 'CREATE_PIXELS',
            pixels: this.state.pixelsArray
        });

        this.websocket.send(message);
        this.setState({mouseDown: false, pixelsArray: []});
    };

    changeColorHandler = event => {
        this.setState({color: event.target.value});
    };

    render() {
        return (
            <div>
                <canvas
                    id="myCanvas"
                    style={{border: '1px solid black'}}
                    width={800}
                    height={600}
                    onMouseDown={this.mouseDownHandler}
                    onMouseUp={this.mouseUpHandler}
                    onMouseMove={this.canvasMouseMoveHandler}
                />
                <div className="colorPicker">
                    <b>Выберите цвет: </b>
                    <input type="color" onChange={this.changeColorHandler} value={this.state.color}/>
                </div>
            </div>
        );
    }
}

export default App;
